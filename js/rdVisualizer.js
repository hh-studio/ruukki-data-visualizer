function rdVisualizer() {

    this.getHelp = () => {
        console.log("Help is WIP");
    };

    this.getSeries = () => {
        return series;
    };

    this.getDataYear = () => {
        return dataYear;
    };

    this.getParseCSVResults = () => {
        return parseCSVResults;
    };

    this.getSeries = () => {
        return series;
    };

    this.getWeatherAverages = () => {
        return weatherAverages;
    };

    this.getParseWeatherResults = () => {
        return parseWeatherResults;
    };

    //vars here are private to class
    var series = {
        yearLabels: [],
        monthLabels: [],
        dayLabels: [],
        year: [],
        months: [],
        days: [],
        dataLoaded: false
    };
    var averages = [];
    var weatherAverages = {};
    var dataYear = "";
    var parseCSVResults = "";
    var parseWeatherResults = "";



    this.showYear = () => {
        if(series.dataLoaded) {
            console.log("Showing year data");
            this.drawChart(series.year, series.yearLabels);
        } else {
            this.showDataLoadError();
        }
    };

    this.showMonth = (monthNumber) => {
        if(series.dataLoaded) {
            console.log("Showing month data: " + monthNumber);
            this.drawChart(series.months[monthNumber], series.monthLabels[monthNumber]);
        } else {
            this.showDataLoadError();
        }
    };

    this.showDate = (date) => {
        if(series.dataLoaded) {
            console.log("Showing day data: " + date);
            var date = date.split("-");
            const month = parseInt(date[1]);
            const day = parseInt(date[2]);
            this.drawChart(series.days[month][day], series.dayLabels[month][day]);
        } else {
            this.showDataLoadError();
        }
    };

    this.init = (data) => {
        console.log(data);
        this.parseCSV(data);
        this.getHelp();
    };

    this.clearWeatherData = () => {
        localStorage.removeItem("weather" + dataYear);
    };

    this.genWeatherAverages = () => {
        var itemKey = "weather" + dataYear;
        var parsed = JSON.parse(localStorage.getItem(itemKey));
        parseWeatherResults = parsed;
        weatherAverages = {};
        if(!parsed) {
            this.downloadWeather();
            return;
        }
        parsed.FeatureCollection.member.forEach((entry, index) => {
            var date = entry.BsWfsElement.Time.__text.split("T")[0].split("-");
            var year = date[0];
            var month = date[1];
            if(month != "10") {
                month = month.replace("0", "");
            }
            var day = date[2];
            if(day != "10" && day != "20" && day != "30") {
                day = day.replace("0", "");
            }
            //console.log(series.days[month]);
            //console.log(day);
            // series.days[month][day][5].data.push(parseInt(entry.BsWfsElement.ParameterValue.__text));
            if(!weatherAverages[entry.BsWfsElement.Time.__text.split("T")[0]]) {
                weatherAverages[entry.BsWfsElement.Time.__text.split("T")[0]] = [];
            }
            var temp = parseInt(entry.BsWfsElement.ParameterValue.__text);
            if(!isNaN(temp)) {
                weatherAverages[entry.BsWfsElement.Time.__text.split("T")[0]].push(temp);
            }
        });
        Object.keys(weatherAverages).forEach((entry, index) => {
            var length = weatherAverages[entry].length;
            var sum = weatherAverages[entry].reduce((a, b) => {
                return a + b;
            });
            weatherAverages[entry] = sum / length;
            series.year[5].data.push(weatherAverages[entry]);
        });

        Object.keys(weatherAverages).forEach((entry, index) => {
            var splitEntry = entry.split("-");
            var month = splitEntry[1];
            var day = splitEntry[2];
            if(month != "10") {
                month = month.replace("0", "");
            }
            if(day != "10" && day != "20" && day != "30") {
                day = day.replace("0", "");
            }
            //console.log(weatherAverages[entry]);
            for(var i = 0; i < 288; i++) {
                series.days[month][day][5].data.push(weatherAverages[entry]);
            }
        });

    };


    this.downloadWeather = () => {
        var xhttp = new XMLHttpRequest();
        var location = "hämeenlinna";
        var apikey = "07146f39-6958-4ea5-8016-d2bc6e38a9df";
        var requestURL = "http://data.fmi.fi/fmi-apikey/" +
            apikey +
            "/wfs?" +
            "request=getFeature&storedquery_id=fmi::observations::" + 
            "weather::daily::simple&place=" +
            location +
            "&starttime=" +
            dataYear +
            "-01-01T00:00:00Z&endtime=" +
            dataYear +
            "-12-31T00:00:00Z";
        xhttp.onreadystatechange = (event) => {
            if (event.currentTarget.readyState == 4 && event.currentTarget.status == 200) {
                var xmlParser = new X2JS();
                var parsed = JSON.stringify(xmlParser.xml_str2json(event.target.response));
                parseWeatherResults = parsed;
                var itemKey = "weather" + dataYear;
                localStorage.setItem(itemKey, parsed);
                console.log("Weather data acquired");
                this.genWeatherAverages();
            }
        };
        xhttp.open("GET", requestURL, true);
        xhttp.send();
    };

    this.genSeriesBase = () => {
        return [
            {
                label: "Irradiation Sensor(Säteilymittaus)",
                data: [],
                backgroundColor: "rgba(255, 255, 51, 0.4)"
            },
            {
                label: "Input(S3 Varaaja meno)",
                data: [],
                backgroundColor: "rgba(0, 255, 0, 0.4)"
            },
            {
                label: "Output(S4 Varaaja paluu)",
                data: [],
                backgroundColor: "rgba(0, 0, 255, 0.4)"
            },
            {
                label: "Outside temperature",
                data: [],
                backgroundColor: "rgba(255, 128, 0, 0.4)"
            },
            {
                label: "Flow rate sensor(FR13 Virtausmittaus)",
                data: [],
                backgroundColor: "rgba(178, 102, 255, 0.4)"
            },
            {
                label: "Weather(Average)",
                data: [],
                backgroundColor: "rgba(255, 51, 51, 0.4)"
            }
        ];
    };

    this.showDataLoadError = () => {
        alert("Please load the data first");
    };

    this.parseCSV = (data) => {
        return Papa.parse(data, {
            complete: (results) => {
                //console.log(results);
                parseCSVResults = results;
                dataYear = results.data[2][0].split(" ")[0].split("/")[2];
                this.yearDataAverages(results.data);
                this.genWeatherAverages(dataYear);
            }
        });
    };



    this.drawChart = (d, labels) => {
        var cc = document.getElementById("canvas-container");
        cc.innerHTML = "";
        var newCanvas = document.createElement("canvas");
        newCanvas.id = "visualization-canvas";
        cc.appendChild(newCanvas);
        this.ctx = newCanvas.getContext("2d");

        var myChart = new Chart(this.ctx, {
            type: "line",
            data: {
                labels: labels,
                datasets: d
            },
            xAxisID: "Time/Date",
            options: {
                responsive: true,
                scales: {
                yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }],
                    xAxes: [{
                        ticks: {
                            beginAtZero: false
                        }
                    }]
                },
                pan: {
                    enabled: true,
                    mode: "xy"
                },
                zoom: {
                    enabled: true,
                    mode: "xy"
                }
            }
        });
    };

    this.pushDataFromEntryToSeries = (entry, series) => {
        series[0].data.push(parseInt(entry[1]));
        series[1].data.push(parseInt(entry[9]));
        series[2].data.push(parseInt(entry[10]));
        series[3].data.push(parseInt(entry[11]));
        series[4].data.push(parseInt(entry[12]));
    };


    this.genSeries = (d) => {
        series.year = this.genSeriesBase();
        Object.keys(d).forEach((year, yindex) => {
            d[year].forEach((month, mindex) => {
                month.forEach((day, dindex) => {
                    if(day) {
                        //Get months data
                        if(!series.months[mindex]) {
                            series.months[mindex] = this.genSeriesBase();
                        }
                        this.pushDataFromEntryToSeries(day, series.months[mindex]);

                        if(!series.monthLabels[mindex]) {
                            series.monthLabels[mindex] = [];
                        }
                        series.monthLabels[mindex][dindex - 1] = dindex;

                        //Year data
                        this.pushDataFromEntryToSeries(day, series.year);
                        series.yearLabels.push(dindex + "/" + mindex);

                    }
                });
            });
        });
        series.dataLoaded = true;
        this.showYear();
    };

    this.yearDataAverages = (data) => {
        //Time format int data is mm/dd/yyyy
        data.forEach((entry, index, arr) => {
            var year = entry[0].split("/")[2];
            if(year) {
                year = year.split(" ")[0];
            }
            var month = entry[0].split("/")[0];
            if(month.charAt(0) == "0") {
                month = month.replace("0", "");
            }
            var day = entry[0].split("/")[1];


            if(!averages[year]) {
                averages[year] = [];
            }

            if(!averages[year][month]) {
                averages[year][month] = [];
            }

            if(!averages[year][month][day]) {
                averages[year][month][day] = [];
                Object.keys(data[0]).forEach((e) => {
                    averages[year][month][day][e] = [];
                    averages[year][month][day][e]["average"] = "";
                });
            }

            //Add day data to series
            if(!series.days[month]) {
                series.days[month] = [];
            }
            if(!series.days[month][day]) {
                series.days[month][day] = this.genSeriesBase();
            }

            this.pushDataFromEntryToSeries(entry, series.days[month][day]);

            if(!series.dayLabels[month]) {
                series.dayLabels[month] = [];
            }
            if(!series.dayLabels[month][day]) {
                series.dayLabels[month][day] = [];
            }
            series.dayLabels[month][day].push(entry[0]);

            Object.keys(data[0]).forEach((e) => {
                averages[year][month][day][e].push(entry[e]);
            });
        });

        Object.keys(data[0]).forEach((e) => {
            Object.keys(averages).forEach((year, yindex) => {
                Object.keys(averages[year]).forEach((month) => {
                    Object.keys(averages[year][month]).forEach((day) => {
                        averages[year][month][day][e] =
                            averages[year][month][day][e]
                            .reduce((a, b) => { return parseInt(a) + parseInt(b); })
                            / parseInt(averages[year][month][day][e].length);
                    });
                });
            });
        });
        this.genSeries(averages);
    };

};
